const esprima = require("esprima");
const estraverse = require("estraverse");

export const parseCode = code => {
  const ast = esprima.parse(code);
  const packages = [];
  estraverse.traverse(ast, {
    enter: function(node) {
      if (node.type === "CallExpression" && node.callee.name === "require") {
        packages.push(node.arguments[0].value);
      }
    }
  });
  return packages.filter(str => !str.startsWith("."));
};
