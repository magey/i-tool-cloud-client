// ws 辅助函数
import { v4 as uuidv4 } from "uuid";

export const getfns = ws => {
  ws.sendSocketMessage({
    data: JSON.stringify({
      CommandText: "iTool Cloud Get Functions",
      Token: uuidv4(),
      Handel: "Command",
      Parameter: JSON.stringify({
        rows: 20,
        page: 1,
        sidx: "id",
        sord: "asc"
      })
    })
  });
};

export const subscribeCommand = ws => {
  ws.sendSocketMessage({
    data: JSON.stringify({
      CommandText: "iTool Subscribe Channel",
      Handel: "Command",
      IsMustBeServed: true,
      Parameter: "TestNotify"
    })
  });
};

export const onMsg = ws => {
  return new Promise(resolve => {
    ws.onSocketMessage(resolve);
  });
};
