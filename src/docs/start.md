# iTool Cloud Framework

一个开源的 FaaS (Function as a Service) 框架
框架由三个部分组成
  - iTool Cloud Service  云服务层
  - iTool Cloud Socket   云通信层
  - iTool Cloud Client   云客户端

[服务端源码仓库地址](https://gitee.com/magey/iTool-cloud-framework)
[客户端源码仓库地址](https://gitee.com/HeartDream/itool-cloud-client)

## 介绍

过去十年来，我们已经把应用和环境中很多通用的部分变成了服务。Serverless的出现，带来了跨越式变革。Serverless把主机管理、操作系统管理、资源分配、扩容，甚至是应用逻辑的全部组件都外包出去，把它们看作某种形式的商品——厂商提供服务，我们掏钱购买。

但是由于云服务发展事件较短，并不适用我们的所有开发场景。前期希望产品快速上线而选择云服务但是后期业务量数据量一旦起来，会发现数据库操作慢了，复杂的业务场景云开发环境难以胜任，优化空间有限。计算成本昂贵，迁移很难...等等

传统的FaaS存在哪些缺点?我认为最主要的两个方面是：
  1. 完全依赖于第三方服务
  2. 无状态

iTool云框架解决了什么问题？
  1. 私有化部署，完全不依赖第三方。并且可定制化扩展优化
  2. 提更云函数的状态解决方案
  3. 提更高并发高可用的解决方案
  4. 自动集群集群负载均衡/注册发现/分布式计算
  5. 安装配置简单，运维成本低


## 快速部署(Windows)
准备工作：
  1. 安装 [Docker](https://file.help-itool.com/Content/Docker.rar)，配置国内镜像
    
    - Settings > Docker Engine > 
    ``` json
    {
      "registry-mirrors": [
        "https://4s82cvz7.mirror.aliyuncs.com"
      ],
      "insecure-registries": [],
      "debug": true,
      "experimental": true
    }
    ```

获取Docker本地服务地址

``` javascript
cmd > ipconfig
// 这里是你的服务地址
vEthernet (Default Switch):
IPv4 地址 . . . . . . . . . . . . : 192.168.1.65
```

### 安装Redis
docker pull redis
docker run -p 6379:6379 -d redis:latest redis-server

### 安装Rabbitmq
docker pull rabbitmq:management
docker run -d --hostname my-rabbit --name rabbit -e RABBITMQ_DEFAULT_USER=admin -e RABBITMQ_DEFAULT_PASS=admin -p 15672:15672 -p 5672:5672 rabbitmq:management

### 安装Mssql
docker pull microsoft/mssql-server-linux
docker run -e ACCEPT_EULA=Y -e MSSQL_PID="Developer" -e MSSQL_SA_PASSWORD="PassWord320" -e MSSQL_TCP_PORT=2433 -p 2433:2433 --name sqlserver -d microsoft/mssql-server-linux

### iTool Cloud Service
docker pull magey/itoolcloudservice
docker run --name itoolcloudservice -p 9635:9635 -d magey/itoolcloudservice -sql:192.168.1.65,2433@sa,PassWord320 -rabbit:192.168.1.65,5672@admin,admin -redis:192.168.1.65:6379 -debug:true -clusterid:iToolServiceCluster -serviceId:iToolService@1526,1245 -dashboard:9635@admin,admin

### iTool Cloud Socket
docker pull magey/itoolcloudsocket
docker run --name itoolcloudsocket -p 9010:9010 -d magey/itoolcloudsocket -sql:192.168.1.65,2433@sa,PassWord320 -rabbit:192.168.1.65,5672@admin,admin -redis:192.168.1.65:6379 -service:iToolService@iToolServiceCluster -netty:9010@01_socket_service -heartbeat:10

服务地址：`192.168.1.65:9010`

### iTool Cloud Socket

下载：[itool-cloud-client](https://gitee.com/HeartDream/itool-cloud-client)

进入：src/main.js
编辑服务地址：
```java
Vue.use(WsPlugin, {
  heartbeatEnable: true,
  autoConnect: false,
  url: "ws://192.168.1.65:9010"
});
```

配置完成 
```java
npm i
npm run server
``` 
启动服务