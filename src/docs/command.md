### 命令介绍

#### [用户身份模块] Channel

1. iTool Bind Channel <font color=red>(Client)</font>
    > 绑定当前连接身份关键字

    ``` javascript
    // parameter
    {
        CommandText: "iTool Bind Channel",
        Handel: "Command",
        Parameter: "{mxf}"
    }
    ```

2. iTool Bind ChannelInfo <font color=red>(Client)</font>
    > 绑定当前连接额外属性

    ``` javascript
    // parameter
    {
        CommandText: "iTool Bind ChannelInfo",
        Handel: "Command",
        Parameter: JSON.stringify({
            name: 'mxf',
            age: 27,
            xxx...
        })
    }
    ```

#### ClouFunction

1. iTool Cloud Run {modulePath} <font color=red>(Client)</font>
    > 调用云函数

    ``` javascript
    // parameter
    {
        CommandText: "iTool Cloud Run folder1/fun1",
        Handel: "Command",
        Parameter: JSON.stringify({
            Parameters:[args...]
        })
    }
    ```

2. iTool Cloud Update {modulePath}
    > 修改云函数

    ``` javascript
    // parameter
    {
        CommandText: "iTool Cloud Update folder1/fun1",
        Handel: "Command",
        Parameter: JSON.stringify({
            Packages:[md5,xxx...],
            ScriptContext: "module.exports = function(xxx){ }"
        })
    }
    ```

3. iTool Cloud Remove {modulePath}
    > 删除云函数

    ``` javascript
    // parameter
    {
        CommandText: "iTool Cloud Remove folder1/fun1",
        Handel: "Command"
    }
    ```

4. iTool Cloud Add Function
    > 添加云函数

    ``` javascript
    // parameter
    {
        CommandText: "iTool Cloud Add Function",
        Handel: "Command",
        Parameter: JSON.stringify({
            ModuleName: "folder1/fun1",
            Packages:[md5,xxx...],
            ScriptContext: "module.exports = function(xxx){ }"
        })
    }
    ```

5. iTool Cloud Get Functions
    > 获取所有云函数（目前分页参数不生效）

    ``` javascript
    // parameter
    {
        CommandText: "iTool Cloud Get Functions",
        Handel: "Command",
        Parameter: JSON.stringify({
            page: 1
        })
    }
    ```

5. iTool Cloud Get {modulePath}
    > 获取指定云函数详细信息

    ``` javascript
    // parameter
    {
        CommandText: "iTool Cloud Get folder1/fun1",
        Handel: "Command"
    }
    ```

#### [数据库API模块] APIJSON <font color=red>(Client)</font>

1. iTool APIJSON

    > 获取指定云函数详细信息

    [语法示例](https://vincentcheng.github.io/apijson-doc/zh/grammar.html#%E6%9F%A5%E8%AF%A2)

    ``` javascript
    // parameter
    {
        CommandText: "iTool APIJSON",
        Handel: "Command",
        Parameter: JSON.stringify({
            Script: "{操作脚本}",
            MothodType:"get | register | modify | remove",
            Connaction:{
                Title: "添加数据库的 {数据库名称}"
            }
        })
    }
    ```

#### [订阅模块] Subscribe <font color=red>(Client)</font>

> 以下操作必须发生在绑定Channel之后

1. iTool Subscribe Channel
    > 订阅指定Channel

    ``` javascript
    // parameter
    {
        CommandText: "iTool Subscribe Channel",
        Handel: "Command",
        Parameter: "{topic}"
    }
    ```

2. iTool UnSubscribe Channel
    > 取消订阅指定Channel

    ``` javascript
    // parameter
    {
        CommandText: "iTool UnSubscribe Channel",
        Handel: "Command",
        Parameter: "{topic}"
    }
    ```

#### [会话管理模块] NotifyMessage <font color=red>(Client)</font>

> 以下操作必须发生在绑定Channel之后

1. iTool Get Message
    > 获取与指定Channel消息内容模块，消息获取排序方式为按时间倒序排序

    ``` javascript
    // parameter
    {
        CommandText: "iTool Get Message",
        Handel: "Command",
        Parameter: JSON.stringify({
            IsGroup: true,
            Pagination: {
                page: 1...
            },
            ReciveChannel: "{当前聊天窗口的Channel}",
            MinDateTime: "{获取到分页消息的最小时间，获取第一页使用当前时间}",
            MinHistoryDateTime?: "{本地消息的最小时间}}" // 可空对象 没有不用传
        })
    }
    ```

2. iTool Get Sesstion
    > 获取当前连接的 Sesstion会话列表

    ``` javascript
    // parameter
    {
        CommandText: "iTool Get Sesstion",
        Handel: "Command"
    }
    ```

3. iTool Sesstion IntoChannel
    > 进入某会话列表

    ``` javascript
    // parameter
    {
        CommandText: "iTool Sesstion IntoChannel",
        Handel: "Command",
        Parameter: "{channel}"
    }
    ```

3. iTool Sesstion OutChannel
    > 退出某会话列表

    ``` javascript
    // parameter
    {
        CommandText: "iTool Sesstion OutChannel",
        Handel: "Command",
        Parameter: "{channel}"
    }
    ```

4. iTool Sesstion RemoveSesstionByChannel
    > 把指定的Channel从会话列表移除

    ``` javascript
    // parameter
    {
        CommandText: "iTool Sesstion RemoveSesstionByChannel",
        Handel: "Command",
        Parameter: "{channel}"
    }
    ```
