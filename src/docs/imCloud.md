# imCloud 使用手册

> 提供了 常用的 方法，及 订阅

```javascript
import imCloud from 'imCloud.js'
// 初始化实例
const instance = imCloud({
  heartbeatEnable: true,
  url: "ws://www.janjanle.com:9010"
})
// 实例上 提供了相关的api 方法
instance.bindIdentityId('hello').then(res => {
  console.log(res)
})
// 1. 用户身份模块
function bindIdentityId(id: string): Promise {}  // 绑定当前连接身份关键字
function bindIdentityInfo(info: {}): Promise {} // 绑定当前连接额外属性

// 2. 云函数模块
// 调用云函数
function invokeMethod(name: string, args: any = []): Promise {}  
// 调用有状态的云函数，route = "folder/file?mothodName"
function invokeMethod(route: string, args: any = [], stateKey: string): Promise {} 

function updateFunction(name: string, scriptcontent: string): Promise {} // 修改云函数
function deleteFunction(name: string): Promise {} // 删除云函数
function addFunction(name: string, scriptcontent: string): Promise {} // 添加云函数
function getFunctions(pageOptions = {}): Promise {} // 获取所有云函数（目前分页参数不生效）
function getFunctionDetail(name: string): Promise {} // 获取指定云函数详细信息

// api json 模块
function invokeApiJson(tbname: string, sql: string, type: 'get' | 'register' | 'modify' | 'remove'): Promise {} // 数据库 处理

// 会话模块
function getMessage(pageInfo = {}, channel: string, MinDateTime: string, MinHistoryDateTime = ''): Promise {} // 获取与指定Channel消息内容模块，消息获取排序方式为按时间倒序排序
function getSession(): Promise {} // 获取当前连接的 Sesstion会话列表
function intoChannel(channel: string): Promise {} // 进入某会话列表
function outChannel(channel: string): Promise {} // 退出某会话列表
function deleteChannel(channel: string): Promise {} // 把指定的Channel从会话列表移除

// 订阅模块
function on(topic: string, callback: any): void {} // 订阅指定Channel
function off(topic: string): void {} // 取消订阅指定Channel
```
