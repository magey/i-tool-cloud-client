import router from "@/router/index";
const userModule = {
  namespaced: true,
  state: () => ({
    userInfo: null,
    token: "",
    isSubscribe: false
  }),
  mutations: {
    setUserInfo(state, data) {
      state.userInfo = data;
      sessionStorage.setItem("userInfo", JSON.stringify(data));
    },
    setToken(state, token) {
      state.token = token;
      sessionStorage.setItem("token", token);
    }
  },
  actions: {
    async LoginIn({ commit }, loginInfo) {
      // const res = await login(loginInfo)
      commit("setUserInfo", loginInfo);
      commit("setToken", Math.random());
      router.push({ path: "/welcome" });
    }
  },
  getters: {
    userInfo() {
      var userInfoStr = sessionStorage.getItem("userInfo");
      if (userInfoStr) {
        return JSON.parse(userInfoStr);
      }
      return null;
      // return state.userInfo;
    },
    isLogin() {
      return Boolean(sessionStorage.getItem("token"));
    },
    token(state) {
      return state.token;
    },
    isSubscribeEditFn(state) {
      return state.isSubscribe;
    },
    subscribeEditFn(state) {
      state.isSubscribe = true;
    }
  }
};
export default userModule;
