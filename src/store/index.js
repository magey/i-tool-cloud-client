import Vue from "vue";
import Vuex from "vuex";
import functionModule from "./function";
import userModule from "./user";
Vue.use(Vuex);
const store = new Vuex.Store({
  modules: {
    function: functionModule,
    user: userModule
  }
});
export default store;
