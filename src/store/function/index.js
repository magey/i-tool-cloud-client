const functionModule = {
  namespaced: true,
  state: () => ({
    name: "hello",
    functions: {}
  }),
  mutations: {
    upateFunc(state, data) {
      state.functions[data.id] = data;
    },
    getFunc(state, data) {
      data.cb(state.functions[data.id]);
    }
  },
  actions: {},
  getters: {}
};
export default functionModule;
