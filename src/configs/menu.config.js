module.exports = [
  {
    iconname: "area-chart",
    menulabel: "Dashboard",
    route: "/welcome"
  },
  {
    iconname: "cloud-upload",
    menulabel: "云函数",
    route: "/functions"
  },
  {
    iconname: "cloud-sync",
    title: "计划任务",
    route: "/reminder",
    children: [
      {
        iconname: "ordered-list",
        menulabel: "任务列表",
        route: "/reminder"
      },
      {
        iconname: "form",
        menulabel: "添加",
        route: "/reminder/add"
      }
    ]
  },
  {
    iconname: "database",
    title: "数据库",
    route: "/database",
    children: [
      {
        iconname: "ordered-list",
        menulabel: "数据库列表",
        route: "/database"
      },
      {
        iconname: "form",
        menulabel: "添加",
        route: "/database/add"
      }
    ]
  },
  {
    iconname: "folder-open",
    title: "文档",
    route: "/docs",
    children: [
      {
        iconname: "file-text",
        menulabel: "Command事件",
        route: "/docs/command"
      },
      {
        iconname: "file-text",
        menulabel: "cloud介绍",
        route: "/docs/cloud"
      }
    ]
  }
  // {
  //   iconname: "video-camera",
  //   title: "websocket示例",
  //   children: [
  //     {
  //       iconname: "video-camera",
  //       menulabel: "指令发送订阅",
  //       route: "/"
  //     }
  //   ]
  // }
];
