/**
 * 状态云函数基本模板
 */
export const baseTemplate = `/***
* iTool Code have state template (Title)
* Author：Jian
* Date：2020/08/30
* Description： iTool This is cloud function.
*/

module.exports =
{
   /***
    * 业务函数部分
    * ...
    */

   // 初始化状态信息
   ReadState:function(key)
   {
       // 构造状态信息。
       // 状态信息初始化，去除了Channel，Session 相关对象
       // 状态应该存储关键信息,大量无效状态信息导致内存的不合理分配
       this.state = { name: key };
   },

   // 状态被更新触发
   WriteState: function(state)
   {
       /***
        * 持久化状态逻辑
        * this.state
        * 状态信息持久化，去除了Channel，Session 相关对象
        */
   }
};`;
/**
 * 状态函数
 */
export const stateTemplate = `/***
* iTool Code have state template (Title)
* Author：Jian
* Date：2020/08/30
* Description： iTool This is cloud function.
*/

module.exports =
{
   /***
    * 业务函数部分
    * ...
    */

   Demo: function(arg1) {
       /***
        * this 对象说明
        *      isWriteState  : bool         - 标记任务完成后是否执行'WriteState', default: false
        *      state         : object       - ReadState() 返回对象
        *      channel       : object       - 当前 Channel 信息
        */

       // 更新状态
       this.state.name = this.state.name + "_x";

       // 完成后调用 'WriteState' 
       this.isWriteState = true;

       // 构造Responses返回值
       return {
           name: this.state.name,
           channelKey: this.channel.key,
           parameter: arg1
       };
   },

   // 初始化状态信息
   ReadState:function(key)
   {
       // 构造状态信息。
       // 状态信息初始化，去除了Channel，Session 相关对象
       // 状态应该存储关键信息,大量无效状态信息导致内存的不合理分配
       this.state = { name: key };
   },

   // 状态被更新触发
   WriteState: function(state)
   {
       /***
        * 持久化状态逻辑
        * this.state
        * 状态信息持久化，去除了Channel，Session 相关对象
        */
   }
};`;

/**
 * 云函数介绍模板
 */
export const descriptionTemplate = `/***
* iTool Code template (Title)
* Author：Jian
* Date：2020/08/30
* Description： iTool This is cloud function.
*/

/***
* 函数类型分2种：
*  1. 无状态
*  2. 有状态
*
*  关于“无状态函数”
*      - 函数每一次运行都会创建一个全新的对象，多次运行结果没有必然关系
*
*  关于“有状态函数”
*      - 函数需要实现（ReadState/WriteState）接口。函数在第一次调用之前会触发 ‘ReadState’ 方法，并且将返回值存储在内存中生命周期由框架管理。
*        该模块之后调用该Module内函数，则会自动注入 State 信息用于业务处理。建议作用与 “触发频繁” “性能要高” 的操作
*
*  同一函数永远都不会并发执行，由框架保证函数的执行顺序。 单函数运行时间为5ms QPS: 1000/5 = 200
*
*  框架具备自动化管理
*      1. 负载均衡
*      2. 注册发现
*      3. 高性能/高容错/高可用
*/

// 导入 npm 包
const packgName = require('sha1');
// 导入其它业务模块，只支持相对路径
const AppSetting = require('./AppSetting');
const Global = require('./Global');

// 用户登录示例
module.exports = function (channel, name) {
    this.channel.key = channel;
    this.channel.parameter = {
      name: name
    };
    return "登录成功";
}

// 退出登录示例
module.exports = function () {
    this.ClearChannel();
    return "登录成功";
}

// Debug示例
module.exports = function (arg1, arg2) {
    console.log(arg1)
    console.log(arg1)
    return arg1 + arg2;
}

// 向客户端推送消息示例
module.exports = function (topic, message) {
    console.log(topic)
    console.log(message)
    this.Notify(topic, message);
    return "推送成功";
}

// 设置会话sesstion
module.exports = function (name) {
    this.sesstion.name = name;
    return "设置成功";
}

// 使用会话sesstion
module.exports = function () {
    return this.sesstion.name;
}


// 无状态函数示例
module.exports = function (number1,number2) {
   return number1 + number2;
}

// 有状态示例
module.exports =
{

   Demo: function (arg1) {

       /***
        * this 对象说明
        *      isWriteState  : bool         - 标记任务完成后是否执行'WriteState', default: false
        *      state         : object       - ReadState() 返回对象
        *      channel       : object       - 当前 Channel 信息
        */

       console.log('调试信息最终会输出在调试面板');

       // 更新状态
       this.state.name = this.state.name + "_x";

       // 完成后调用 'WriteState'
       this.isWriteState = true;

       return {
           channelKey: this.channel.key,
           arg1: arg1,
           channelParameter: this.channel.parameter
       };

   },

   // 初始化状态信息
   ReadState: function (key) {
       // 构造状态信息。
       // 状态信息初始化，去除了Channel，Session 相关对象
       // 状态应该存储关键信息,大量无效状态信息导致内存的不合理分配
       this.state = { name: key };
   },

   // 状态被更新触发
   WriteState: function (state) {
       /***
        * 持久化状态逻辑
        * state
        * 状态信息持久化，去除了Channel，Session 相关对象
        */
   }
};`;

/**
 * 普通云函数
 */
export const normalTemplate = `module.exports = function(arg1) {

  if(!this.channel.key){
      return "用户未登录";
  }else{
      return {arg1};
  }
}`;
/**
 * 系统配置模板
 */
export const configTemplate = `/***
* 云函数系统模块
*/

module.exports = {
}`;
