import Vue from "vue";
import App from "./App.vue";
import router from "@/router";
// import WsPlugin from "@/plugins";
import store from "@/store";
import "prismjs/themes/prism.css";
import MdComponent from "@/components/mdpreview.js";
import imCloud from "@/assets/js/imCloud.js";

import {
  Layout,
  Menu,
  Icon,
  Input,
  Table,
  Button,
  Tag,
  Divider,
  Upload,
  message,
  Tree,
  Form,
  Select,
  Spin,
  Alert,
  Modal,
  Drawer,
  Tooltip,
  Tabs,
  Avatar
} from "ant-design-vue";
// Vue.use(JsonViewer);
Vue.use(Layout);
Vue.use(Menu);
Vue.use(Icon);
Vue.use(Input);
Vue.use(Table);
Vue.use(Button);
Vue.use(Tag);
Vue.use(Divider);
Vue.use(Upload);
Vue.use(Tree);
Vue.use(Select);
Vue.use(Spin);
Vue.use(Alert);
Vue.use(Modal);
Vue.use(Drawer);
Vue.use(Tooltip);
Vue.use(Form);
Vue.use(Tabs);
Vue.use(Avatar);
Vue.component("md", MdComponent);

Vue.prototype.$message = message;
Vue.prototype.$confirm = Modal.confirm;
Vue.prototype.$info = Modal.info;

const SockPlugin = (_, options) => {
  Vue.prototype.$cloud = imCloud(options);
};

Vue.use(SockPlugin, {
  heartbeatEnable: true,
  url: "ws://www.janjanle.com:9010"
  // url: "ws://192.168.1.65:9010"
  // url: "ws://127.0.0.1:9010"
  // url: "ws://localhost:64055"
  // url: 'ws://4wqn1t4v.duankouyingshe.net:15252'
});

// Vue.use(WsPlugin, {
//   heartbeatEnable: true,
//   autoConnect: true,
//   // url: "ws://127.0.0.1:9010"
//   // url: "ws://192.168.1.65:9010"
//   // url: "ws://8lljrfai.xiaomy.net:37134"
//   url: "ws://www.janjanle.com:9010"
//   // url: "wss://cloud.itool.store:9011"
// });

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
