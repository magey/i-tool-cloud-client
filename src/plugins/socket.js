class SocketUtil {
  wsInfo = null;
  constructor(wsInfo) {
    this.wsInfo = wsInfo;
  }
  connect() {
    this.wsInfo._connect();
  }
  isOpen() {
    return this.wsInfo.isOpen;
  }
}
export default SocketUtil;
