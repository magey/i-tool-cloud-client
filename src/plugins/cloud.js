class CloudUtil {
  wsInfo = null;
  constructor(wsInfo) {
    this.wsInfo = wsInfo;
  }
  _invoke(options) {
    return this.wsInfo._send(options);
  }
  // 执行 sql
  runSql(options) {
    return this.wsInfo._send({
      CommandText: "iTool Excuted Sql",
      Handel: "Command",
      Parameter: JSON.stringify(options)
    });
  }
  getClouds(options = {}) {
    return this.wsInfo._send({
      CommandText: "iTool Cloud Get Functions",
      Handel: "Command",
      Parameter: JSON.stringify(options)
    });
  }
  getCloud(modulename) {
    return this.wsInfo._send({
      CommandText: `iTool Cloud Get ${modulename}`,
      Handel: "Command"
    });
  }
  removeCloud(modulename) {
    return this.wsInfo._send({
      CommandText: `iTool Cloud Remove ${modulename}`
    });
  }
  // 添加云函数
  addCloud(modulename, options = {}) {
    const Parameter = {
      ...options,
      ModuleName: modulename
    };
    return this.wsInfo._send({
      CommandText: "iTool Cloud Add Function",
      Parameter: JSON.stringify(Parameter)
    });
  }
  // 更新云函数
  updateCloud(modulename, options = {}) {
    const Parameter = {
      ...options,
      ModuleName: modulename
    };
    return this.wsInfo._send({
      CommandText: `iTool Cloud Update ${modulename}`,
      Parameter: JSON.stringify(Parameter)
    });
  }
  runCommand(CommandText, options = {}) {
    return this.wsInfo._send({
      CommandText,
      Handel: "Command",
      Parameter: typeof options !== "string" ? JSON.stringify(options) : options
    });
  }
  // 运行 云函数 无状态
  runCloud(options = {}) {
    let CommandText = "";
    if (!options.CommandText) {
      return Promise.reject("CommandText 不可为空");
    } else {
      CommandText = options.CommandText;
    }
    delete options.CommandText;
    return this.wsInfo._send({
      CommandText,
      Handel: "Command",
      Parameter: JSON.stringify(options)
    });
  }
  runCloudWithName(modulename, options = {}) {
    return this.wsInfo._send({
      CommandText: `iTool Cloud Run ${modulename}`,
      Handel: "Command",
      Parameter: JSON.stringify(options)
    });
  }
  // 运行 云函数 有状态
  runCloudWithState(modulename, state = "", fnname = "", options = {}) {
    const Parameter = {
      ...options,
      IsUseState: !!state,
      StateKey: state,
      ExportedFunctionName: fnname
    };
    return this.wsInfo._send({
      CommandText: `iTool Cloud Run ${modulename}`,
      Handel: "Command",
      Parameter: JSON.stringify(Parameter)
    });
  }
}
export default CloudUtil;
