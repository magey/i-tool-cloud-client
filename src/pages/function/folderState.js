const subTopic = "edit_functions";

const broadcastUtil = function(action, parameter) {
  parameter.isBroadcast = true;
  this.$cloud
    .notify({
      ToChannel: subTopic,
      Parameter: JSON.stringify({
        action: action,
        parameter
      })
    })
    .then(res => {
      if (res == 1) {
        console.log("消息推送成功");
      } else {
        console.error("消息推送失败");
      }
    });
};

const commandUtil = function(commandText, parameter) {
  this.$cloud
    .command(commandText, parameter)
    .then(res => {
      console.log(res);
    })
    .catch(error => {
      console.error(error);
    });
};

export const subscribeTopic = subTopic;

export const getFileByModulePath = function(modulePath, folders) {
  if (this) {
    if (this.fileKeyValueDic[modulePath]) {
      return this.fileKeyValueDic[modulePath];
    }
  }

  let res = null;
  folders[0].childs.forEach(element => {
    if (res !== null) {
      return;
    }
    if (element.modulePath == modulePath) {
      res = {
        parent: folders[0],
        element: element
      };
      return;
    }
    element.childs.forEach(file => {
      if (file.modulePath == modulePath) {
        res = {
          parent: element,
          element: file
        };
        return;
      }
    });
  });
  return res;
  // for (let index = 0; index < folders.length; index++) {
  //   const oFile = folders[index];
  //   // childs
  //   if (oFile.modulePath == modulePath) {
  //     return oFile;
  //   } else if (oFile.childs) {
  //     return getFileByModulePath(modulePath, oFile.childs);
  //   }
  // }
};

// 操作狀態同步
export const synchro = {
  // 同步進入文件編輯狀態
  doEditState(parameter) {
    let file = this.getOFileByModulePaht(parameter.modulePath);
    file.isEdit = true;
    file.isReadOnly = parameter.editUserName != this.userinfo.name;
    file.editUserName = parameter.editUserName;
  },
  // 同步退出文件編輯狀態
  unEditState(parameter) {
    let file = this.getOFileByModulePaht(parameter.modulePath);
    file.isEdit = false;
    file.isReadOnly = false;
    file.editUserName = "";
  },
  // 同步文件信息
  // TODU 打开某个文件，在监听某个文件的数据。 每次打开文件都向服务器获取最新的数据
  fileContent(parameter) {
    const obj = getFileByModulePath.bind(this)(
      parameter.modulePath,
      this.folders
    );
    const file = obj.element;
    file.isReadOnly = parameter.editUserName != this.userinfo.name;
    file.editUserName = parameter.editUserName;

    if (parameter.editUserName != this.userinfo.name) {
      if (file.isReadOnly && file.editor) {
        file.editor.setValue(parameter.changeValue);
      }
    }

    console.log("fileContent", parameter, obj);
  },
  // 同步添加文件
  addFile(parameter) {
    this.handleOk(parameter);
  },
  // 同步添加文件夾
  addFolder(parameter) {
    this.handleOk(parameter);
  },
  // 同步刪除文件
  delFile(parameter) {
    const obj = getFileByModulePath(parameter.modulePath, this.folders);
    if (obj) {
      const parent = obj.parent;
      for (let index = 0; index < parent.childs.length; index++) {
        const element = parent.childs[index];
        if (element.modulePath == obj.element.modulePath) {
          parent.childs.splice(index, 1);
        }
      }
    }
  },
  // 同步刪除文件夾
  delFolder(parameter) {
    const obj = getFileByModulePath(parameter.modulePath, this.folders);
    if (obj) {
      const parent = obj.parent;
      for (let index = 0; index < parent.childs.length; index++) {
        const element = parent.childs[index];
        if (element.modulePath == obj.element.modulePath) {
          parent.childs.splice(index, 1);
        }
      }
    }
  }
};

// 操作廣播
const grain = {
  addFile(parameter) {
    commandUtil.bind(this)("iTool Cloud AddTemporaryFunction", {
      IsUseState: parameter.selectedValue == "有状态",
      ModuleName: parameter.selectedChildName + "/" + parameter.modalInput
    });
  },

  fileContent(parameter) {
    if (parameter.changeValue) {
      commandUtil.bind(this)("iTool Cloud EnterEdit " + parameter.modulePath, {
        ScriptContext: parameter.changeValue
      });
    }
  },

  unEditState(parameter, delayed) {
    setTimeout(
      function() {
        commandUtil.bind(this)("iTool Cloud ExitEdit " + parameter.modulePath, {
          ScriptContext: parameter.changeValue
        });
      }.bind(this),
      delayed
    );
  }
};
export const broadcast = {
  // 進入文件編輯狀態
  doEditState(file, userInfo) {
    file.isEdit = true;
    file.editUserName = userInfo.name;
    broadcastUtil.bind(this)("doEditState", {
      modulePath: file.modulePath,
      editUserName: userInfo.name
    });
  },
  // 退出文件編輯狀態
  unEditState(file, userInfo, changeValue, delayed) {
    console.log("unEditState", file.editUserName, userInfo.name, changeValue);
    debugger;
    if (file.editUserName != userInfo.name) {
      return;
    }
    file.isEdit = false;
    file.isReadOnly = false;
    file.editUserName = "";
    broadcastUtil.bind(this)("unEditState", {
      modulePath: file.modulePath
    });
    grain.unEditState.bind(this)(
      {
        modulePath: file.modulePath,
        changeValue: changeValue
      },
      delayed
    );
  },
  // 同步添加文件
  addFile(parameter) {
    console.log("obj,", parameter);
    broadcastUtil.bind(this)("addFile", parameter);
    grain.addFile.bind(this)(parameter);
  },
  // 同步添加文件夾
  addFolder(parameter) {
    broadcastUtil.bind(this)("addFolder", parameter);
  },
  // 同步刪除文件
  delFile(modulePath) {
    // TODU
    broadcastUtil.bind(this)("delFile", { modulePath });
  },
  // 同步刪除文件夾
  delFolder(modulePath) {
    // TODU
    broadcastUtil.bind(this)("delFolder", { modulePath });
  },
  // 同步文件信息
  fileContent(parameter) {
    parameter.isBroadcast = true;
    parameter.editUserName = this.userinfo.name;
    console.log("广播fileContent", parameter);
    // 给其它客户端广播文件同步信息
    // broadcastUtil.bind(this)("fileContent", parameter);
    // 给后台同步数据
    grain.fileContent.bind(this)(parameter);
  }
};
