export default {
  props: {
    url: String
  },
  computed: {
    viewName() {
      const url = this.url.replace("@/docs", "");
      return () => import(`@/docs${url}`);
    }
  },
  render(h) {
    return h(this.viewName);
  }
};
