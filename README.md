# itool-cloud-client

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



1. [服务端 - iTool-Cloud-Framework](https://gitee.com/magey/iTool-cloud-framework/tree/structure)

2. [客户端 - iTool-Cloud-Client](https://gitee.com/magey/i-tool-cloud-client)

3. [网关 - iTool-Gateway](https://gitee.com/magey/itool-gateway)

