const path = require("path");
const CompressionWebpackPlugin = require("compression-webpack-plugin");
const HardSourceWebpackPlugin = require("hard-source-webpack-plugin");
const AddAssetHtmlPlugin = require("add-asset-html-webpack-plugin");
const MonacoEditorWebpackPlugin = require("monaco-editor-webpack-plugin");
const webpack = require("webpack");

module.exports = config => {
  if (process.env.NODE_ENV === "production") {
    const plugins = [
      new CompressionWebpackPlugin({
        filename: "[path].gz[query]",
        algorithm: "gzip",
        test: /\.(js|css|json|txt|ico|svg)(\?.*)?$/i,
        threshold: 0,
        minRatio: 0.8,
        deleteOriginalAssets: true
      }),
      new webpack.DllReferencePlugin({
        context: path.join(__dirname, "../"),
        name: "thirdlibrary",
        manifest: require("../vendor-manifest.json")
      }),
      new HardSourceWebpackPlugin(),
      new AddAssetHtmlPlugin({
        // dll文件位置
        filepath: path.resolve(__dirname, "../dlls/*.js"),
        // dll 引用路径
        publicPath: "./dist",
        // dll最终输出的目录
        outputPath: "./dist"
      })
    ];
    config.plugins.concat(plugins);
  }
  config.resolve.alias["@"] = path.join(__dirname, "../src");
  config.module.rules.push({
    test: /\.md$/,
    use: [
      {
        loader: "vue-loader"
      },
      {
        loader: require.resolve("./markdown-loader")
      }
    ]
  });
  config.plugins.push(
    new MonacoEditorWebpackPlugin({
      languages: ["javascript", "typescript", "json", "sql", "python"],
      features: ["coreCommands", "find"]
    })
  );
};
