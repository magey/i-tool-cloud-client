const path = require("path");
const webpack = require("webpack");
module.exports = {
  // 你想要打包的模块的数组
  entry: {
    vendor: ["ant-design-vue", "vue-monaco"]
  },
  output: {
    path: path.join(__dirname, "dlls"), // 打包后文件输出的位置
    filename: "[name].dll.js",
    library: "thirdlibrary"
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.(ttf|eot|svg|woff|woff2)$/,
        use: "url-loader"
      }
    ]
  },
  plugins: [
    new webpack.DllPlugin({
      path: path.resolve(__dirname, "[name]-manifest.json"),
      name: "thirdlibrary",
      context: __dirname
    })
  ]
};
