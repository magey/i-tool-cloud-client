const configureWebpack = require("./scripts/customwebpack");

module.exports = {
  productionSourceMap: false,
  publicPath: "/itool-cloud-client",
  css: {
    loaderOptions: {
      less: {
        javascriptEnabled: true
      }
    }
  },
  devServer: {
    host: "0.0.0.0",
    disableHostCheck: true,
    port: 3600
  },
  configureWebpack
};
